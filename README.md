PyTorch框架下，使用MNIST数据集训练生成对抗网络（GAN）。

# GAN 训练流图

<div style="text-align: center;">
<img src="./figures/training-strategy.png" alt="">
</div>

# 参考文献
1. [MNIST: Generative Adverserial Networks in PyTorch](https://www.kaggle.com/code/kmldas/mnist-generative-adverserial-networks-in-pytorch)
